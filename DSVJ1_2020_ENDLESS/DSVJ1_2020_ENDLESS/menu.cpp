#include "menu.h"

using namespace endless;
using namespace music;


namespace endless {

	namespace menu {			

		gameMusic* gameMenuMusic;

		void init() {
			gameMenuMusic = new gameMusic("res/music/Future-Bass-Menu.mp3");
			gameMenuMusic->setMyVolumen(0.35f);
			gameMenuMusic->playMusic();
			gameMenuMusic->loopMusic();
		}
		void update() {
			button::update();
			gameMenuMusic->updateMusic();
		}
		void draw() {
			button::draw();
		}
		void deinit() {

		}
	}
}