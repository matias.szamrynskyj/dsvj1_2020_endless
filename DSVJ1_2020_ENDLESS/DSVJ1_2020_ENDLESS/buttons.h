#ifndef BUTTONS_H
#define	BUTTONS_H

#include <iostream>
#include "raylib.h"
#include "gameResolution.h"
#include "play.h"
#include "scenes.h"
#include "sounds.h"
#include "music.h"
#include "menu.h"

namespace endless {

	namespace button {

		class buttons {
		private:
			Texture2D myTexture;		
		public:
			buttons();
			buttons(const char* dir,int i, int width, int height);
			~buttons();
			void loadTexture(const char* fileName);
			void loadTexture(Texture2D texture);
			int getHeight();
			int getWidth();			
			Texture2D getTexture();		
		};

		extern buttons* restartButton[2];
		extern buttons* resumeButton[2];
		extern buttons* menuButton[2];
		extern buttons* backButton[2];

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif