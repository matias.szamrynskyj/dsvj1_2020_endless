#ifndef TUTORIAL_H
#define TUTORIAL_H

#include "raylib.h"
#include "buttons.h"
#include "scenes.h"
#include "sounds.h"
#include "music.h"

namespace endless {

	namespace tutorial {		

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif

