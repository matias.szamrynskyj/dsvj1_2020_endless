#include "sun.h"

namespace endless {

	namespace sun {		
		gameSun::gameSun() {
			myTexture = LoadTexture("res/assets/suns/sun0.png");
			posX = GetScreenWidth() - 150.0f;
			posY = 0.0f;
			position = { posX, posY };
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };			
		}

		gameSun::gameSun(int i, int width, int height) {
			myTexture = gameResolution::resize(FormatText("res/assets/suns/sun%i.png", i), width, height);
			UnloadImage(gameResolution::texture);
			posX = GetScreenWidth() - 150.0f;
			posY = 5.0f;
			position = { posX, posY };
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };
		}

		gameSun::~gameSun() {

		}

		void gameSun::setPosition(float posY) {
			position.y += posY;
		}

		float gameSun::getPosX() {
			return position.x;
		}

		float gameSun::getPosY() {
			return position.y;
		}

		float gameSun::getFrameX() {
			return frameRec.x;
		}

		float gameSun::getFrameY() {
			return frameRec.y;
		}

		float gameSun::getTextureWidth() {
			return myTexture.width;
		}

		float gameSun::getTextureHeight() {
			return myTexture.height;
		}		

		void gameSun::draw() {

			DrawTextureRec(myTexture, frameRec, position, WHITE);
		}

		gameSun* mySuns[5];	

		void init() {
			for (int i = 0; i < cant; i++)
				mySuns[i] = new gameSun(i, 100,100);			
		}

		void update() {

		}

		void draw() {
			//for (int i = 0; i < cant; i++)
				mySuns[0]->draw();			
		}

		void deinit() {
			for (int i = 0; i < cant; i++) {
				delete mySuns[i];
				mySuns[i] = NULL;
			}
		}
	}
}