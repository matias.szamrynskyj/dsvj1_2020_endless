#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "raylib.h"
#include "gameResolution.h"

namespace endless {

	namespace background {

		class gameBackGround {
		private:
			Texture2D myTexture;
			Vector2 position;
			Rectangle frameRec;
			int speed;
			float startPosY;

		public:
			gameBackGround();
			~gameBackGround();
			void loadTexture(const char* fileName);
			void loadTexture(Texture2D texture);
			int getHeight();
			int getWidth();
			Texture2D getTexture();
		};
	}
}
#endif

