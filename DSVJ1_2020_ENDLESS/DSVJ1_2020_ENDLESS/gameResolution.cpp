#include "gameResolution.h"

namespace endless {

	namespace gameResolution {

		gameRes res800x600;
		gameRes res1024x768;
		gameRes res1280x720;
		gameRes res1920x1080;

		int screenWidth = 800;
		int screenHeight = 600;

		Image texture;

		Texture2D resize(const char* dir, int newWidth, int newHeight) {
			texture = LoadImage(dir);
			ImageResize(&texture, newWidth, newHeight);
			return LoadTextureFromImage(texture);
		}

		//void resizeVars(int& var,int newSize, int originalSize1, int originalSize2) {
		//	var = (newSize * originalSize1) / originalSize2;
		//}

		void init() {
			res800x600 = gameRes::r800x600;
			res1024x768 = gameRes::r1024x768;
			res1280x720 = gameRes::r1280x720;
			res1920x1080 = gameRes::r1920x1080;
		}

		void update() {


		}
		void draw() {

		}

		void deinit() {

		}
	}
}