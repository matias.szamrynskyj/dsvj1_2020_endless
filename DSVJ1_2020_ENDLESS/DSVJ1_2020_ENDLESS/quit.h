#ifndef QUIT_H
#define QUIT_H

#include "game.h"
#include "buttons.h"
#include "scenes.h"
#include "sounds.h"

namespace endless {

	namespace quit {


		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif // !QUIT_H
