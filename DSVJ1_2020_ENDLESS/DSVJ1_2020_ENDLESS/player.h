#ifndef PLAYER_H
#define PLAYER

#include "raylib.h"
#include "gameResolution.h"
#include "highway.h"

namespace endless {

	namespace player {

		class player {
		private:
			Texture2D myTexture;			
			Vector2 position;
			Rectangle frameRec;
			int lives;
			int points;
			int speed;
		public:
			player();
			void loadTexture(Texture2D texture);

			void moveUp();
			void moveDown();			
			float getY();

			Texture2D getTexture();
			Rectangle getRec();
			
			void drawPlayer();
		};				

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
