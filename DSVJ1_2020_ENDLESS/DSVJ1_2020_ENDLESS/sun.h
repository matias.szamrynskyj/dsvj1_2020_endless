#ifndef SUN_H
#define SUN_H

#include "raylib.h"
#include "gameResolution.h"

namespace endless {

	namespace sun {
		class gameSun{
		private:
			Texture2D myTexture;
			Vector2 position;
			Rectangle frameRec;			
			float posX;
			float posY;

		public:
			gameSun();
			gameSun(int i, int width, int height);
			~gameSun();

			void setPosition(float posY);				

			float getPosX();
			float getPosY();
			float getFrameX();
			float getFrameY();
			float getTextureWidth();
			float getTextureHeight();					

			void draw();
		};

		const int cant = 5;

		extern gameSun* mySuns[cant];

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif // !SUN_H



