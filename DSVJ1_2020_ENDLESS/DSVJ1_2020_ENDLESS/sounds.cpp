#include "sounds.h"

namespace endless {

	namespace sounds {

		sounds::sounds(const char* fileName) {
			setMySound(fileName);
		}

		sounds::~sounds() {
			UnloadSound(gameSound);
		}

		void sounds::setMySound(const char* fileName) {
			gameSound = LoadSound(fileName);
		}

		void sounds::playMySound() {
			PlaySound(gameSound);
		}

		void sounds::setMyVolumen(float vol = 1.0f) {
			SetSoundVolume(gameSound, vol);
		}

		sounds* pressButtonSFX;
		sounds* changeButtonSFX;
		//sounds* lostSFX;

		void init() {
			InitAudioDevice();
			pressButtonSFX = new sounds("res/SFX/PressedButtonSfx.mp3");
			changeButtonSFX = new sounds("res/SFX/ChangeButtonSfx.mp3");

			pressButtonSFX->setMyVolumen(0.6f);
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {
			delete pressButtonSFX;
			delete changeButtonSFX;
			CloseAudioDevice();
		}
	}
}