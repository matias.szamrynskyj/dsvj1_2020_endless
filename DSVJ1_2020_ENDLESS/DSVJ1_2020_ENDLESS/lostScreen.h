#ifndef LOSTSCREEN_H
#define LOSTSCREEN_H

namespace endless {

	namespace gameLostScreen {
		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif


