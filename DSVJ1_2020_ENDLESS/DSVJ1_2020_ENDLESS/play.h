#ifndef PLAY_H
#define PLAY_H

#include "raylib.h"
#include "scenes.h"
#include "player.h"
#include "highway.h"
#include "sun.h"
#include "buttons.h"
#include "scenes.h"
#include "sounds.h"
#include "music.h"

namespace endless {

	namespace play {
		extern bool pause;
		extern bool playing;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
