#ifndef CREDITS_H
#define CREDITS_H

#include "raylib.h"
#include "scenes.h"
#include "buttons.h"
#include "sounds.h"

namespace endless {

	namespace credits {

		class gameCredits{

		};

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
