#include "player.h"

using namespace  endless;
using namespace  highway;

namespace endless {

	namespace player {

		player::player() {
			lives = 1;
			points = 0;
			speed = 165;
			myTexture = gameResolution::resize("res/assets/autos/player.png", 120, 55);
			UnloadImage(gameResolution::texture);
			position = { 10.0f, gameHighway[0]->getStartPosY()};
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };
		}

		void player::loadTexture(Texture2D texture) {
			myTexture = texture;
		}

		void player::moveUp()
		{
			if (position.y > highway::gameHighway[0]->getPosY())
				if (IsKeyDown(KEY_W))
					position.y -= speed * GetFrameTime();			
		}

		void player::moveDown()
		{
			if (position.y + myTexture.height < highway::gameHighway[2]->getPosY() + highway::gameHighway[0]->getTextureHeight())
				if (IsKeyDown(KEY_S))
					position.y += speed * GetFrameTime();
		}

		float player::getY(){
			return position.y;
		}

		Texture2D player::getTexture() {
			return myTexture;
		}

		Rectangle player::getRec()
		{
			return frameRec;
		}


		void player::drawPlayer()
		{
			#if _DEBUG
			DrawRectangleLines(static_cast<int>(position.x), static_cast<int>(position.y), myTexture.width, myTexture.height, LIME);
			#endif // _DEBUG

			DrawTextureRec(myTexture, frameRec, position, WHITE);
		}

		player* myPlayer;

		void init() {
			myPlayer = new player();
		}

		void update() {
			myPlayer->moveUp();
			myPlayer->moveDown();
			
		}

		void draw() {
			myPlayer->drawPlayer();
		}

		void deinit() {
			delete myPlayer;
			myPlayer = NULL;
		}

	}
}