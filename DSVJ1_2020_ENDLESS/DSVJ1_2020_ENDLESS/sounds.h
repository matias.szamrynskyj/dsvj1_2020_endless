#ifndef SOUNDS_H
#define SOUNDS_H

#include "raylib.h"

namespace endless {

	namespace sounds {

		class sounds {
		private:
			Sound gameSound;
		public:
			sounds(const char* fileName);
			~sounds();
			void setMySound(const char* fileName);
			void playMySound();		
			void setMyVolumen(float vol);
		};

		extern sounds* pressButtonSFX;
		extern sounds* changeButtonSFX;
		//

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif