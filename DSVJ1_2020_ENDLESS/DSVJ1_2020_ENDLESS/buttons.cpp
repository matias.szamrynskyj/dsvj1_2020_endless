#include "buttons.h"

using namespace endless;
using namespace scenes;
using namespace sounds;

namespace endless {

	namespace button {

		buttons::buttons() {

		}

		buttons::buttons(const char* dir, int i, int width, int height) {
			myTexture = gameResolution::resize(FormatText(dir, i), width, height);
			UnloadImage(gameResolution::texture);
		}

		buttons::~buttons() {

		}

		void buttons::loadTexture(const char* fileName) {
			myTexture = LoadTexture(fileName);
		}
		void buttons::loadTexture(Texture2D texture) {
			myTexture = texture;
		}

		int buttons::getHeight() {
			return myTexture.height;
		}

		int buttons::getWidth() {
			return myTexture.width;
		}

		Texture2D buttons::getTexture() {
			return myTexture;
		}

		int resX = 400;
		int resY = 120;

		int posX = 200;
		int posY = 15;
		int OffSet = 150;

		buttons* playButton[2];
		buttons* tutorialButton[2];
		buttons* creditsButton[2];
		buttons* exitButton[2];
		buttons* backButton[2];
		buttons* restartButton[2];
		buttons* resumeButton[2];
		buttons* menuButton[2];

		int num;

		void init() {

			for (int i = 0; i < 2; i++) {
				playButton[i] = new buttons("res/assets/menu/playButton%i.png", i, resX, resY);
				tutorialButton[i] = new buttons("res/assets/menu/tutorialButton%i.png", i, resX, resY);
				creditsButton[i] = new buttons("res/assets/menu/creditsButton%i.png", i, resX, resY);
				exitButton[i] = new buttons("res/assets/menu/exitButton%i.png", i, resX, resY);
				backButton[i] = new buttons("res/assets/menu/backButton%i.png", i, resX / 2 - 40, resY / 2);
				restartButton[i] = new buttons("res/assets/menu/restartButton%i.png", i, resX, resY);
				resumeButton[i] = new buttons("res/assets/menu/resumeButton%i.png", i, resX, resY);
				menuButton[i] = new buttons("res/assets/menu/menuButton%i.png", i, resX, resY);
			}

			num = 0;
		}

		void update() {

			if (IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP))
				if (num > 0) {
					num--;
					changeButtonSFX->playMySound();
				}					

			if (IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN))
				if (num < 3) {
					num++;
					changeButtonSFX->playMySound();
				}

			switch (num)
			{
			case 0:
				if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
					currentScene = Scene::play;
					play::playing = true;
					pressButtonSFX->playMySound();
					music::gameMusic1->playMusic();
					music::gameMusic1->loopMusic();
					menu::gameMenuMusic->stopMusic();
				}
				break;

			case 1:
				if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
					currentScene = Scene::tutorial;
					pressButtonSFX->playMySound();	
					menu::gameMenuMusic->resumeMusic();
				}
					

				break;

			case 2:
				if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
					currentScene = Scene::credits;
					pressButtonSFX->playMySound();
					menu::gameMenuMusic->resumeMusic();
				}
				

				break;

			case 3:
				if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
					currentScene = Scene::quit;
					pressButtonSFX->playMySound();
					menu::gameMenuMusic->resumeMusic();
				}
				break;
			}
		}

		void draw() {

			switch (num)
			{
			case 0:
				if (num != 0)
					DrawTexture(playButton[0]->getTexture(), posX, posY, WHITE);
				else
					DrawTexture(playButton[1]->getTexture(), posX, posY, WHITE);
					DrawTexture(tutorialButton[0]->getTexture(), posX, posY + OffSet, WHITE);
					DrawTexture(creditsButton[0]->getTexture(), posX, posY + OffSet * 2, WHITE);
					DrawTexture(exitButton[0]->getTexture(), posX, posY + OffSet * 3, WHITE);
				break;

			case 1:
				if (num != 1)
					DrawTexture(tutorialButton[0]->getTexture(), posX, posY + OffSet, WHITE);
				else
					DrawTexture(playButton[0]->getTexture(), posX, posY, WHITE);
					DrawTexture(tutorialButton[1]->getTexture(), posX, posY + OffSet, WHITE);
					DrawTexture(creditsButton[0]->getTexture(), posX, posY + OffSet * 2, WHITE);
					DrawTexture(exitButton[0]->getTexture(), posX, posY + OffSet * 3, WHITE);
				break;

			case 2:
				if (num != 2)
					DrawTexture(creditsButton[0]->getTexture(), posX, posY + OffSet * 2, WHITE);
				else
					DrawTexture(playButton[0]->getTexture(), posX, posY, WHITE);
					DrawTexture(tutorialButton[0]->getTexture(), posX, posY + OffSet, WHITE);
					DrawTexture(creditsButton[1]->getTexture(), posX, posY + OffSet * 2, WHITE);
					DrawTexture(exitButton[0]->getTexture(), posX, posY + OffSet * 3, WHITE);
				break;

			case 3:
				if (num != 3)
					DrawTexture(exitButton[0]->getTexture(), posX, posY + OffSet * 3, WHITE);
				else
					DrawTexture(playButton[0]->getTexture(), posX, posY, WHITE);
					DrawTexture(tutorialButton[0]->getTexture(), posX, posY + OffSet, WHITE);
					DrawTexture(creditsButton[0]->getTexture(), posX, posY + OffSet * 2, WHITE);
					DrawTexture(exitButton[1]->getTexture(), posX, posY + OffSet * 3, WHITE);
				break;
			}
		}

		void deinit() {

			for (int i = 0; i < 2; i++) {
				delete playButton[i];
				delete tutorialButton[i];
				delete creditsButton[i];
				delete exitButton[i];
				delete backButton[i];
				delete restartButton[i];

				playButton[i] = NULL;
				tutorialButton[i] = NULL;
				creditsButton[i] = NULL;
				exitButton[i] = NULL;
				backButton[i] = NULL;
				restartButton[i] = NULL;
			}

		}
	}
}