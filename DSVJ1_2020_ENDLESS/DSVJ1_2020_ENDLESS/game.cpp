#include "game.h"

using namespace endless;
using namespace scenes;

namespace endless {

	namespace game {				
		bool exitGame = false;

		void run() {
			init();
			//SetWindowMonitor(0);
			while (!WindowShouldClose() && exitGame != true){
				update();
				draw();
			}
			deinit();
		}

		static void init() {
			InitWindow(gameResolution::screenWidth, gameResolution::screenHeight, "ENDLESS");

			SetTargetFPS(60);
			SetExitKey(KEY_DELETE);

			scenes::currentScene = Scene::menu;
			exitGame = false;

			menu::init();
			sounds::init();
			music::init();
			button::init();
			quit::init();
			highway::init();
			sun::init();
			player::init();
		}

		static void update() {
			switch (scenes::currentScene)
			{
			case Scene::menu:
				menu::update();			
				break;

			case Scene::play:
				play::update();				
				break;

			case Scene::tutorial:
				tutorial::update();				
				break;

			case Scene::credits:
				credits::update();				
				break;

			case Scene::quit:
				quit::update();				
				break;

			case Scene::lostScene:
				gameLostScreen::update();
				break;
			}
		}

		static void draw() {
			BeginDrawing();
			ClearBackground(BLACK);

			switch (scenes::currentScene)
			{
			case Scene::menu:
				menu::draw();
				break;

			case Scene::play:
				play::draw();
				break;

			case Scene::tutorial:
				tutorial::draw();
				break;

			case Scene::credits:
				credits::draw();
				break;

			case Scene::quit:
				quit::draw();
				break;

			case Scene::lostScene:
				gameLostScreen::draw();
				break;
			}
			EndDrawing();
		}

		static void deinit() {
		
			CloseWindow();
		}
	}
}