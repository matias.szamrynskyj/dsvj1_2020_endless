#include "quit.h"

using namespace endless;
using namespace button;
using namespace scenes;
using namespace sounds;

namespace endless {

	namespace quit {

		buttons* yesButton[2];
		buttons* noButton[2];
		int num;
		
		int noYesButtonResX = 200;
		int noYesButtonResY = noYesButtonResX;
		
		int posX = 150;
		int posY = GetScreenHeight() + 200;
		int OffSet = 300;

		void init() {
			for (int i = 0; i < 2; i++)	{			
				yesButton[i] = new buttons("res/assets/menu/yesButton%i.png", i, noYesButtonResX, noYesButtonResY);
				noButton[i] = new buttons("res/assets/menu/noButton%i.png", i, noYesButtonResX, noYesButtonResY);
			}
			
			num = 0;			
		}

		void update() {

			menu::gameMenuMusic->updateMusic();

			if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT))
				if (num > 0)
					num--;
			
			if (IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT))
				if (num < 1)
					num++;
			
			switch (num){
			case 0:
				if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {					
					game::exitGame = true;
				}
				break;
			
			case 1:
				if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
					currentScene = Scene::menu;
					pressButtonSFX->playMySound();
				}
				break;
			}
		}


		void draw() {
			switch (num){
			case 0:
				if (num != 0)
					DrawTexture(yesButton[0]->getTexture(), posX, posY, WHITE);
				else
					DrawTexture(yesButton[1]->getTexture(), posX, posY, WHITE);
					DrawTexture(noButton[0]->getTexture(), posX + OffSet, posY, WHITE);
				break;
			
			case 1:
				if (num != 1)
					DrawTexture(noButton[0]->getTexture(), posX + OffSet, posY, WHITE);
				else				
					DrawTexture(noButton[1]->getTexture(), posX + OffSet, posY, WHITE);
					DrawTexture(yesButton[0]->getTexture(), posX, posY, WHITE);
				break;
			}
		}

		void deinit() {
			for (int i = 0; i < 2; i++)	{
				delete yesButton[i];
				delete noButton[i];
			
				yesButton[i] = NULL;
				noButton[i] = NULL;
			}		
		}
	}
}