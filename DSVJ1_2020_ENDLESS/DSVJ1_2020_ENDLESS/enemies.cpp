#include "enemies.h"

using namespace  endless;


namespace endless {

	namespace enemies {

		enemies::enemies() {
			myTexture = gameResolution::resize("res/assets/autos/auto%i.png", 120, 55);
			UnloadImage(gameResolution::texture);
			speed = 50.0f;
			startPosX = 0.0f;
			startPosY = 0.0f;
			position = { 0.0f, startPosY };
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };
		}

		enemies::enemies(int i, int width, int height) {
			myTexture = gameResolution::resize(FormatText("res/assets/autos/auto%i.png", i), width, height);
			UnloadImage(gameResolution::texture);
			speed = 50.0f;
			startPosX = 0.0f;
			startPosY = 0.0f;
			position = { 0.0f, startPosY };
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };
		}

		enemies::~enemies() {

		}

		void enemies::loadTexture(Texture2D texture) {
			myTexture = texture;
		}

		void enemies::setSpeed(float _speed) {
			speed = _speed;
		}

		void enemies::movement() {
			if (position.x > GetScreenWidth())				
					position.x -= speed * GetFrameTime();
		}	

		void enemies::setX(float posX) {
			position.x = posX;
		}

		void enemies::setY(float posY) {
			position.y = posY;
		}

		float enemies::getStartPosY()
		{
			return startPosY;
		}

		void enemies::checkCollision(Rectangle player)	{
			if (CheckCollisionRecs(player, frameRec)) {
				//position.y = GetScreenWidth() + 200;
			}
		}

		float enemies::getX() {
			return position.x;
		}

		float enemies::getY() {
			return position.y;
		}

		Texture2D enemies::getTexture() {
			return myTexture;
		}

		void enemies::drawEnemy() {
			#if _DEBUG
			DrawRectangleLines(static_cast<int>(position.x), static_cast<int>(position.y), myTexture.width, myTexture.height, LIME);
			#endif // _DEBUG

			DrawTextureRec(myTexture, frameRec, position, WHITE);
		}

		void init() {

		}

		void update() {

		}

		void draw() {

		}

		void deinit() {

		}

	}
}