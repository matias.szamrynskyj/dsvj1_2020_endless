#include "credits.h"

using namespace endless;
using namespace button;
using namespace scenes;
using namespace sounds;

namespace endless {

	namespace credits {
		void init() {

		}

		void update() {

			menu::gameMenuMusic->updateMusic();

			if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
				currentScene = Scene::menu;
				pressButtonSFX->playMySound();
			}
		}

		void draw() {
			DrawTexture(backButton[1]->getTexture(), GetScreenWidth() - backButton[1]->getWidth(), GetScreenHeight() - backButton[1]->getHeight(), WHITE);
		}

		void deinit() {

		}
	}
}