#ifndef MUSIC_H
#define MUSIC_H

#include "raylib.h"

namespace endless {

	namespace music {
		class gameMusic {
		private:
			Music myMusic;

		public:
			gameMusic(const char* fileName);
			~gameMusic();
			void playMusic();
			void stopMusic();
			void loopMusic();
			void resumeMusic();
			void setMyVolumen(float vol);
			bool isPlaying();
			void updateMusic();
		};

		extern gameMusic* gameMusic1;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif