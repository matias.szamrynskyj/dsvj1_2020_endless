#include "highway.h"

using namespace endless;
using namespace enemies;

namespace endless {

	namespace highway {

		highway::highway() {
			myTexture = gameResolution::resize("res/assets/ruta-2.png", 3840, 110);
			UnloadImage(gameResolution::texture);
			startPosY = 200.0f;			
			position = {0.0f, startPosY };
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };
			speed = 200;
		}

		highway::highway(const char* dir){
			myTexture = gameResolution::resize(dir, 3840, 400);
			UnloadImage(gameResolution::texture);
			startPosY = 250.0f;
			position = { 0.0f, startPosY };
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };
			speed = 200;
		}

		highway::highway(const char* dir, int width, int height){
			myTexture = gameResolution::resize(dir, width, height);
			UnloadImage(gameResolution::texture);
			startPosY = 0.0f;
			position = { 0.0f, startPosY };
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };
			speed = 200;
		}

		highway::~highway() {

		}

		void highway::setPosition(float posY)	{
			position.y += posY;
		}

		void highway::movement()
		{			
			position.x -= speed * GetFrameTime();

			if (position.x < -1920)
				position.x=0;
		}

		void highway::checkCollision()
		{
			
		}

		void highway::setSpeed(int newSpeed)
		{
			speed += newSpeed;
		}

		float highway::getPosX()
		{
			return position.x;
		}

		float highway::getPosY()
		{
			return position.y;
		}

		float highway::getFrameX()
		{
			return frameRec.x;			
		}

		float highway::getFrameY()
		{
			return frameRec.y;
		}

		int highway::getTextureWidth(){
			return myTexture.width;
		}

		int highway::getTextureHeight()
		{
			return myTexture.height;
		}

		float highway::getStartPosY(){
			return startPosY;
		}

		void highway::draw()	{
			#if _DEBUG
			DrawRectangleLines(static_cast<int>(position.x), static_cast<int>(position.y), myTexture.width, myTexture.height, LIME);
			#endif // _DEBUG

			DrawTextureRec(myTexture, frameRec, position, WHITE);
		}

		highway* gameHighway[3];
		highway* grass;
		highway* background;
		highway* sky;

		float offSet1;

		void init() {
			for (int i = 0; i < 3; i++)
				gameHighway[i] = new highway();
			
			grass = new highway("res/assets/grass1.png");
			background = new highway("res/assets/background1.png", 3480, 70);
			sky = new highway("res/assets/sky.png", 1920, 150);

			offSet1 = 140.0f;
			gameHighway[1]->setPosition(offSet1);
			gameHighway[2]->setPosition(offSet1*2);

			gameHighway[1]->setSpeed(100);
			gameHighway[2]->setSpeed(150);

			background->setSpeed(200);
			background->setPosition(150);

			sky->setPosition(0);
		}

		void update() {
			for (int i = 0; i < 3; i++)
				gameHighway[i]->movement();

			grass->movement();
			background->movement();	
		}

		void draw() {

			sky->draw();
			grass->draw();
			background->draw();

			for (int i = 0; i < 3; i++)
				gameHighway[i]->draw();
		}

		void deinit() {
			for (int i = 0; i < 3; i++)	{
				delete gameHighway[i];
				gameHighway[i] = NULL;
			}

			delete grass;
			delete background;
			delete sky;

			grass = NULL;
			background = NULL;
			sky = NULL;
		}

	}
}