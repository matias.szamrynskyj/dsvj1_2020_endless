#include "scenes.h"

namespace endless {

	namespace scenes {

		Scene currentScene;
		Scene menuScreen;
		Scene playScreen;
		Scene tutorialScreen;
		Scene creditsScreen;
		Scene pauseScreen;
		Scene quitScreen;		
		Scene lostScreen;

		void init() {
			menuScreen = Scene::menu;
			playScreen = Scene::play;
			tutorialScreen = Scene::tutorial;
			creditsScreen = Scene::credits;
			pauseScreen = Scene::pauseScreen;
			quitScreen = Scene::quit;			
			lostScreen = Scene::lostScene;
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {

		}

	}
}