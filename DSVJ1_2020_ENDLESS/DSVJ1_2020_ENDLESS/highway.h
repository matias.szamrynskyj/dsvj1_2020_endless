#ifndef HIGHWAY_H
#define HIGHWAY_H

#include "raylib.h"
#include "gameResolution.h"
#include "enemies.h"

namespace endless {

	namespace highway {

		class highway{
		private:
			Texture2D myTexture;
			Vector2 position;
			Rectangle frameRec;			
			int speed;
			float startPosY;
			
		public:
			highway();
			highway(const char* dir);
			highway(const char* dir, int width, int height);
			~highway();
			void setPosition(float posY);
			void movement();
			void checkCollision();

			void setSpeed(int newSpeed);

			float getPosX();
			float getPosY();
			float getFrameX();
			float getFrameY();
			int getTextureWidth();
			int getTextureHeight();
			float getStartPosY();
			//Texture2D getTexture();

			void draw();	
		};

		extern highway* gameHighway[3];		

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif



