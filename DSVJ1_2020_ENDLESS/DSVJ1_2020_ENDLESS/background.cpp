#include "background.h"

namespace endless {

	namespace background {
		
		Texture2D background;

		gameBackGround::gameBackGround() {
			myTexture = gameResolution::resize("res/assets/ruta-2.png", GetScreenWidth(), GetScreenHeight());
			UnloadImage(gameResolution::texture);
			startPosY = 0.0f;
			position = { 0.0f, startPosY };
			frameRec = { 0.0f, 0.0f, (float)myTexture.width, (float)myTexture.height };
			speed = 200.f;
		}

		gameBackGround::~gameBackGround() {

		}	

		void gameBackGround::loadTexture(const char* fileName) {
			myTexture = LoadTexture(fileName);
		}
		void gameBackGround::loadTexture(Texture2D texture) {
			myTexture = texture;
		}

		int gameBackGround::getHeight() {
			return myTexture.height;
		}

		int gameBackGround::getWidth() {
			return myTexture.width;
		}

		Texture2D gameBackGround::getTexture() {
			return myTexture;
		}

	}
}