#include "music.h"

namespace endless {

	namespace music {		

		gameMusic* gameMusic1;		

		gameMusic::gameMusic(const char* fileName) {
			myMusic = LoadMusicStream(fileName);
		}

		gameMusic::~gameMusic()	{
			UnloadMusicStream(myMusic);
		}

		void gameMusic::playMusic() {
			PlayMusicStream(myMusic);
		}

		void gameMusic::stopMusic()	{
			StopMusicStream(myMusic);
		}

		void gameMusic::loopMusic()	{
			SetMusicLoopCount(myMusic, 1);
		}

		void gameMusic::resumeMusic(){
			ResumeMusicStream(myMusic);
		}

		void gameMusic::setMyVolumen(float vol = 1.0f) {
			SetMusicVolume(myMusic, vol);
		}

		bool gameMusic::isPlaying()	{
			return IsMusicPlaying(myMusic);
		}

		void gameMusic::updateMusic()	{
			UpdateMusicStream(myMusic);
		}

		void init() {	
			gameMusic1 = new gameMusic("res/music/Aggressive-Racing.mp3");
			gameMusic1->setMyVolumen(0.3f);	
		}

		void update() {

		}

		void draw() {

		}

		void deinit() {

		}


	}
}