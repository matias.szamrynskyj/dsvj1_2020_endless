#ifndef ENEMIES_H
#define ENEMIES_H

#include "raylib.h"
#include "gameResolution.h"

namespace endless {

	namespace enemies {

		class enemies {
		private:
			Texture2D myTexture;
			Vector2 position;
			Rectangle frameRec;
			float speed;
			float startPosX;
			float startPosY;
		public:
			enemies();
			enemies(int i, int width, int height);
			~enemies();
			void loadTexture(Texture2D texture);

			void setSpeed(float _speed);

			void movement();
			void setX(float posX);
			void setY(float posY);
			float getStartPosY();

			void checkCollision(Rectangle player);

			float getX();
			float getY();

			Texture2D getTexture();

			void drawEnemy();
		};		

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif