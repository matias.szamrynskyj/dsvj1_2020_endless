#include "play.h"

using namespace endless;
using namespace scenes;
using namespace sounds;
using namespace music;

namespace endless {

	namespace play {

		bool pause;
		bool playing;

		int num;

		void init() {
			pause = false;
			playing = false;
			num = 0;
		}

		void update() {

			if (playing) {

				music::gameMusic1->updateMusic();				

				if (IsKeyPressed(KEY_ESCAPE))
					pause = !pause;				

				if (!pause) {
					player::update();
					highway::update();
				}
				else {		

					if (IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP))
						if (num > 0) {
							changeButtonSFX->playMySound();
							num--;
						}

					if (IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN))
						if (num < 2) {
							num++;
							changeButtonSFX->playMySound();
						}

					switch (num)
					{
					case 0:
						if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
							pressButtonSFX->playMySound();
							pause = !pause;
						}
						break;

					case 1:
						if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
							pressButtonSFX->playMySound();
							//restart
						}
						break;

					case 2:
						if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_KP_ENTER)) {
							pressButtonSFX->playMySound();
							playing = false;
							pause = !pause;
							currentScene = Scene::menu;
							music::gameMusic1->stopMusic();	
							menu::gameMenuMusic->playMusic();
						}
						break;
					}
				}
			}
		}

		void draw() {
			if (!pause) {
				highway::draw();
				player::draw();
				sun::draw();
			}
			else {				
				switch (num) {
				case 0:
					if (num != 0)
						DrawTexture(button::resumeButton[0]->getTexture(), GetScreenWidth() / 2 - button::resumeButton[0]->getWidth() / 2, GetScreenHeight() / 2 - 250, WHITE);
					else
						DrawTexture(button::resumeButton[1]->getTexture(), GetScreenWidth() / 2 - button::resumeButton[0]->getWidth() / 2, GetScreenHeight() / 2 - 250, WHITE);
						DrawTexture(button::restartButton[0]->getTexture(), GetScreenWidth() / 2 - button::restartButton[0]->getWidth() / 2, GetScreenHeight() / 2 - 100, WHITE);
						DrawTexture(button::menuButton[0]->getTexture(), GetScreenWidth() / 2 - button::restartButton[0]->getWidth() / 2, GetScreenHeight() / 2 + 50, WHITE);
					break;

				case 1:
					if (num != 1)
						DrawTexture(button::restartButton[0]->getTexture(), GetScreenWidth() / 2 - button::restartButton[0]->getWidth() / 2, GetScreenHeight() / 2 - 100, WHITE);
					else
						DrawTexture(button::restartButton[1]->getTexture(), GetScreenWidth() / 2 - button::restartButton[0]->getWidth() / 2, GetScreenHeight() / 2 - 100, WHITE);
						DrawTexture(button::resumeButton[0]->getTexture(), GetScreenWidth() / 2 - button::resumeButton[0]->getWidth() / 2, GetScreenHeight() / 2 - 250, WHITE);
						DrawTexture(button::menuButton[0]->getTexture(), GetScreenWidth() / 2 - button::restartButton[0]->getWidth() / 2, GetScreenHeight() / 2 + 50, WHITE);
					break;

				case 2:
					if (num != 2)
						DrawTexture(button::menuButton[0]->getTexture(), GetScreenWidth() / 2 - button::menuButton[0]->getWidth() / 2, GetScreenHeight() / 2 + 50, WHITE);
					else
						DrawTexture(button::menuButton[1]->getTexture(), GetScreenWidth() / 2 - button::restartButton[0]->getWidth() / 2, GetScreenHeight() / 2 + 50, WHITE);
						DrawTexture(button::resumeButton[0]->getTexture(), GetScreenWidth() / 2 - button::resumeButton[0]->getWidth() / 2, GetScreenHeight() / 2 - 250, WHITE);
						DrawTexture(button::restartButton[0]->getTexture(), GetScreenWidth() / 2 - button::restartButton[0]->getWidth() / 2, GetScreenHeight() / 2 - 100, WHITE);
					break;
				}
								
				DrawText("PRESS ESCAPE to UNPAUSE", 10, GetScreenHeight() - 25, 20, BLUE);
			}			
			
			DrawFPS(10, 10);
		}

		void deinit() {

		}
	}
}