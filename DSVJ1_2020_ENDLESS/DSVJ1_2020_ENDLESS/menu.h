#ifndef MENU_H
#define MENU_H

#include "buttons.h"
#include "gameResolution.h"
#include "music.h"

namespace endless {

	namespace menu {

		extern music::gameMusic* gameMenuMusic;

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
