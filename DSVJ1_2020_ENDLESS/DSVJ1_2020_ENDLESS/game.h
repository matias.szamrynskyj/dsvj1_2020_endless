#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "scenes.h"
#include "buttons.h"
#include "play.h"
#include "credits.h"
#include "tutorial.h"
#include "menu.h"
#include "quit.h"
#include "lostScreen.h"
#include "sun.h"
#include "music.h"

namespace endless {

	namespace game {

		extern bool exitGame;

		void run();

		static void init();
		static void update();
		static void draw();
		static void deinit();
	}
}
#endif

