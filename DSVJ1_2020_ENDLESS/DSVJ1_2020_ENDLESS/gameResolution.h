#ifndef GAMERESOLUTION_H
#define GAMERESOLUTION_H

#include "raylib.h"

namespace endless {

	namespace gameResolution {
		
		enum class gameRes {
			r800x600 = 0,
			r1024x768,
			r1280x720,
			r1920x1080
		};		

		extern int screenWidth;
		extern int screenHeight;

		extern Image texture;

		extern Texture2D resize(const char* dir, int newWidth, int newHeight);

		//extern void resizeVars(int& var, int newSizeint, int originalSize1, int originalSize2);

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif



